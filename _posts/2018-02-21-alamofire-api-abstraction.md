---
title: 'Alamofire API Abstraction'
categories:
    - gists
tags:
    - iOS
    - Alamofire
    - API
    - VIPER Architecture
    - CocoaPods
---

A sample api abstraction implementation patterned on top of Alamofire

## EndpointProtocol.swift

```swift
import Alamofire

protocol EndpointProtocol:URLRequestConvertible{
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}
```

> It is an optional protocol class but it is recommended.

<!-- more -->

## UserService.swift
```swift
import Alamofire

enum UserService: EndpointProtocol{
    
    case login(login:Login)
    case getUserDetails(id: int)

    var method: HTTPMethod{
        switch self{
        case .login:
            return .post
        case .getUserDetails:
            return .get
        }
    }
    
    var path: String{
        switch self{
        case .login:
            return "api/v1/login/"
        case .getUserDetails:
            return "api/v1/users/"
        }
    }
    
    var parameters: Parameters?{
        switch self{
        case .login(let login):
            return login.toJSON()
        case .getUserDetails(let id):
            return [ "id" : id ]
        }
    }
    
    var headers: HTTPHeaders?{
        switch self {
        case .login( _):
            return nil
        case .getUserDetails( _):
            return [ "Authorization" : API.AUTHORIZATION_KEY ]
        }
    }
        
    func asURLRequest() throws -> URLRequest {
        let url = try API.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // Methods
        urlRequest.httpMethod = method.rawValue
        
        // Headers
        urlRequest.allHTTPHeaderFields = headers
        
        // Returning URL Request with encoding
        switch self {
        case .login:
            return try JSONEncoding.default.encode(urlRequest, with: parameters)
        case .getUserDetails:
            return try URLEncoding.default.encode(urlRequest, with: parameters)
        }
        
    }
    
}
```

## API.swift

```swift
  static let BASE_URL = "http://www.example.com"
  static let AUTHORIZATION_KEY = "example_authorization_key"
```

> Reusability wise, the base url is placed in a separate class.
